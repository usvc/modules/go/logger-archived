package logger

import (
	"net/url"
	"strconv"

	"github.com/evalphobia/logrus_fluent"
	"github.com/sirupsen/logrus"
)

func createFluentHook(fluentURL, defaultTag string) (*logrus_fluent.FluentHook, error) {
	u := url.URL{Host: fluentURL}
	hostname := u.Hostname()
	port, err := strconv.Atoi(u.Port())
	if err != nil {
		return nil, err
	}
	fluentHook, err := logrus_fluent.NewWithConfig(logrus_fluent.Config{
		DefaultMessageField: "@msg",
		DefaultTag:          defaultTag,
		Host:                hostname,
		Port:                port,
	})
	if err != nil {
		return nil, err
	}
	return fluentHook, nil
}

func getFluentFormatCustomiser(timestampFormat string) func(*logrus.Entry, logrus.Fields) {
	return func(entry *logrus.Entry, data logrus.Fields) {
		fieldData := map[string]interface{}{}
		fieldDataCount := 0
		for key, value := range data {
			if key[0] != byte('@') && key != "level" {
				fieldData[key] = value
				fieldDataCount++
			}
		}
		if fieldDataCount > 0 {
			data["@data"] = fieldData
		}
		delete(data, "level")
		data["@level"] = entry.Level.String()
		if entry.HasCaller() {
			function, file := prettifyCaller(entry.Caller)
			data["@func"] = function
			data["@file"] = file
		}
		data["@msg"] = entry.Message
		data["@timestamp"] = entry.Time.Format(timestampFormat)
	}
}

func getFluentLevels() []logrus.Level {
	return []logrus.Level{
		Levels["trace"],
		Levels["info"],
		Levels["debug"],
		Levels["warn"],
		Levels["error"],
		Levels["fatal"],
		Levels["panic"],
	}
}
