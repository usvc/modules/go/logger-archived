package main

import (
	"log"

	"gitlab.com/usvc/modules/go/logger"
)

var envConfig = logger.NewConfigFromEnv("cmd/example")
var logInstance = logger.New(envConfig)

func main() {
	log.Println("start")
	if hookableLogger, ok := logInstance.(logger.HookableLogger); ok {
		log.Println("fluentd is enabled")
		if err := logger.EnableFluent(hookableLogger, envConfig); err != nil {
			log.Printf("error while setting up fluentd remote: '%s'", err)
		}
	}
	defer func() {
		if r := recover(); r != nil {
			defer func() {
				if r := recover(); r != nil {
					log.Println("end")
				}
			}()
			logInstance.Error(r)
			logInstance.Fatalf("hello %s", "world")
		}
	}()
	logInstance.Print("hello world")
	logInstance.Printf("hello %s", "world")
	logInstance.Trace("hello world")
	logInstance.Tracef("hello %s", "world")
	logInstance.Debug("hello world")
	logInstance.Debugf("hello %s", "world")
	logInstance.Info("hello world")
	logInstance.Infof("hello %s", "world")
	logInstance.Warn("hello world")
	logInstance.Warnf("hello %s", "world")
	logInstance.Error("hello world")
	logInstance.Errorf("hello %s", "world")
	a := struct {
		number int
	}{
		number: 1,
	}
	logInstance.Info(a)
	logInstance.(logger.FieldLogger).WithFields(map[string]interface{}{
		"hi": "world",
	}).Info("HELLO")
	logInstance.Info(a)
	logInstance.Fatal("hello world")
}
