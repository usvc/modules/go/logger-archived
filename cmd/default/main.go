package main

import (
	"log"

	"gitlab.com/usvc/modules/go/logger"
)

func main() {
	log.Println("start")
	defer func() {
		if r := recover(); r != nil {
			defer func() {
				logger.Fatal("hello world")
			}()
			logger.Panicf("hello %s", "world")
		}
	}()
	logger.Print("hello world")
	logger.Printf("hello %s", "world")
	logger.Trace("hello world")
	logger.Tracef("hello %s", "world")
	logger.Debug("hello world")
	logger.Debugf("hello %s", "world")
	logger.Info("hello world")
	logger.Infof("hello %s", "world")
	logger.Warn("hello world")
	logger.Warnf("hello %s", "world")
	logger.Error("hello world")
	logger.Errorf("hello %s", "world")
	logger.Panic("hello world")
}
