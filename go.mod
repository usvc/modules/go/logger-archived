module gitlab.com/usvc/modules/go/logger

go 1.12

require (
	github.com/evalphobia/logrus_fluent v0.5.4
	github.com/fluent/fluent-logger-golang v1.4.0 // indirect
	github.com/philhofer/fwd v1.0.0 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/viper v1.4.0
	github.com/tinylib/msgp v1.1.0 // indirect
)
