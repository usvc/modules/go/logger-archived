package logger

import (
	"fmt"
	"path"
	"reflect"
	"runtime"
)

// getEnvLabel retrieves the `env` field tag from a specified field
// :fieldName from the provided type :reflection
func getEnvLabel(reflection reflect.Type, fieldName string) string {
	field, found := reflection.FieldByName(fieldName)
	if !found {
		return fieldName
	}
	return field.Tag.Get("env")
}

// prettifyCaller formats the file/function references given a runtime.Frame
// reference and returns them
func prettifyCaller(frame *runtime.Frame) (string, string) {
	functionName := frame.Function
	fileInfo := fmt.Sprintf("%s:%v", path.Base(frame.File), frame.Line)
	return functionName, fileInfo
}
