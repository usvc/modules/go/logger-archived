dep:
	@GO111MODULE=on go mod vendor -v
	@GO111MODULE=on go mod download

test:
	@go test ./...

default:
	@go run ./cmd/default
default_build:
	@$(MAKE) _cmd_build CMD=default
default_image:
	@docker build \
		--file ./build/docker/Dockerfile \
		--tag usvc/logger-default:latest \
		.

example:
	@go run ./cmd/example
example_with_fluent:
	@FLUENT_URL=localhost:24224 go run ./cmd/example
example_build:
	@$(MAKE) _cmd_build CMD=example
example_image:
	@docker build \
		--file ./build/docker/Dockerfile \
		--tag usvc/logger-example:latest \
		.

_cmd_build:
	@GO111MODULE=on \
		CGO_ENABLED=0 \
		go build -a \
		-mod vendor \
		-ldflags "-extldflags '-static'" \
		-o ./bin/${CMD} \
		./cmd/${CMD}
	@chmod +x ./bin/${CMD}

fluentd:
	@docker build \
		--file ./deployment/fluentd/Dockerfile \
		./deployment/fluentd
fluentd_deploy:
	@$(MAKE) _deploy_fluentd
fluentd_deploy_background:
	@$(MAKE) _deploy_fluentd ARGS="-d"
_fluentd_deploy:
	@docker-compose \
		--file ./deployment/fluentd/docker-compose.yml \
		up ${ARGS} -V
