package logger

import (
	"os"

	"github.com/sirupsen/logrus"
)

// DefaultInstance provides a quick and dirty logrus compliant logger
var DefaultInstance = &logrus.Logger{
	Formatter: &logrus.TextFormatter{
		CallerPrettyfier: prettifyCaller,
		DisableSorting:   true,
		FullTimestamp:    true,
		QuoteEmptyFields: true,
		TimestampFormat:  DefaultTimestampFormat,
	},
	Level:        logrus.TraceLevel,
	Out:          os.Stdout,
	ReportCaller: true,
}

var Print = DefaultInstance.Print
var Printf = DefaultInstance.Printf
var Trace = DefaultInstance.Trace
var Tracef = DefaultInstance.Tracef
var Debug = DefaultInstance.Debug
var Debugf = DefaultInstance.Debugf
var Info = DefaultInstance.Info
var Infof = DefaultInstance.Infof
var Warn = DefaultInstance.Warn
var Warnf = DefaultInstance.Warnf
var Error = DefaultInstance.Error
var Errorf = DefaultInstance.Errorf
var Fatal = DefaultInstance.Fatal
var Fatalf = DefaultInstance.Fatalf
var Panic = DefaultInstance.Panic
var Panicf = DefaultInstance.Panicf
