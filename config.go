package logger

import (
	"reflect"

	"github.com/spf13/viper"
)

const (
	DefaultFieldFile       = "@file"
	DefaultFieldFunc       = "@func"
	DefaultFieldLevel      = "@level"
	DefaultFieldMessage    = "@msg"
	DefaultFieldTimestamp  = "@timestamp"
	DefaultFluentURL       = ""
	DefaultLevel           = "trace"
	DefaultTag             = "log"
	DefaultReportCaller    = true
	DefaultTimestampFormat = "2006-01-02T15:04:05-0700"
)

type Config struct {
	Fields          map[string]interface{}
	FieldFile       string `env:"field_file"`
	FieldFunc       string `env:"field_func"`
	FieldLevel      string `env:"field_level"`
	FieldMessage    string `env:"field_message"`
	FieldTimestamp  string `env:"field_timestamp"`
	FluentURL       string `env:"fluent_url"`
	Level           string `env:"level"`
	Tag             string `env:"tag"`
	TimestampFormat string `env:"timestamp_format"`
	ReportCaller    bool   `env:"report_caller"`
}

func NewConfig(tag ...string) *Config {
	return NewConfigFromEnv(tag...)
}

func NewConfigFromEnv(tag ...string) *Config {
	config := newConfig()
	config.AutomaticEnv()
	if len(tag) > 0 {
		configuration := reflect.TypeOf(Config{})
		config.Set(getEnvLabel(configuration, "Tag"), tag[0])
	}
	return createConfig(config)
}

// newConfig initialises defaults for the configuration and
// returns a spf13/viper configuration map, how to assign the
// custom values is left to the caller.
func newConfig() *viper.Viper {
	config := viper.New()
	configuration := reflect.TypeOf(Config{})
	config.SetDefault(getEnvLabel(configuration, "FieldFile"), DefaultFieldFile)
	config.SetDefault(getEnvLabel(configuration, "FieldFunc"), DefaultFieldFunc)
	config.SetDefault(getEnvLabel(configuration, "FieldLevel"), DefaultFieldLevel)
	config.SetDefault(getEnvLabel(configuration, "FieldMessage"), DefaultFieldMessage)
	config.SetDefault(getEnvLabel(configuration, "FieldTimestamp"), DefaultFieldTimestamp)
	config.SetDefault(getEnvLabel(configuration, "FluentURL"), DefaultFluentURL)
	config.SetDefault(getEnvLabel(configuration, "Level"), DefaultLevel)
	config.SetDefault(getEnvLabel(configuration, "Tag"), DefaultTag)
	config.SetDefault(getEnvLabel(configuration, "TimestampFormat"), DefaultTimestampFormat)
	config.SetDefault(getEnvLabel(configuration, "ReportCaller"), DefaultReportCaller)
	return config
}

// createConfig creates our custom Config structure using the
// provided spf13/viper configuration instance.
func createConfig(config *viper.Viper) *Config {
	configuration := reflect.TypeOf(Config{})
	return &Config{
		Fields:          make(map[string]interface{}),
		FieldFile:       config.GetString(getEnvLabel(configuration, "FieldFile")),
		FieldFunc:       config.GetString(getEnvLabel(configuration, "FieldFunc")),
		FieldLevel:      config.GetString(getEnvLabel(configuration, "FieldLevel")),
		FieldMessage:    config.GetString(getEnvLabel(configuration, "FieldMessage")),
		FieldTimestamp:  config.GetString(getEnvLabel(configuration, "FieldTimestamp")),
		FluentURL:       config.GetString(getEnvLabel(configuration, "FluentURL")),
		Level:           config.GetString(getEnvLabel(configuration, "Level")),
		Tag:             config.GetString(getEnvLabel(configuration, "Tag")),
		TimestampFormat: config.GetString(getEnvLabel(configuration, "TimestampFormat")),
		ReportCaller:    config.GetBool(getEnvLabel(configuration, "ReportCaller")),
	}
}
