package logger

import (
	"errors"
	"os"

	"github.com/sirupsen/logrus"
)

type Logger interface {
	Print(...interface{})
	Printf(string, ...interface{})
	Trace(...interface{})
	Tracef(string, ...interface{})
	Debug(...interface{})
	Debugf(string, ...interface{})
	Info(...interface{})
	Infof(string, ...interface{})
	Warn(...interface{})
	Warnf(string, ...interface{})
	Error(...interface{})
	Errorf(string, ...interface{})
	Fatal(...interface{})
	Fatalf(string, ...interface{})
	Panic(...interface{})
	Panicf(string, ...interface{})
}

type FieldLogger interface {
	WithFields(logrus.Fields) *logrus.Entry
}

type HookableLogger interface {
	AddHook(logrus.Hook)
}

var Levels = map[string]logrus.Level{
	"trace": logrus.TraceLevel,
	"debug": logrus.DebugLevel,
	"info":  logrus.InfoLevel,
	"warn":  logrus.WarnLevel,
	"error": logrus.ErrorLevel,
	"fatal": logrus.FatalLevel,
	"panic": logrus.PanicLevel,
}

func EnableFluent(logger HookableLogger, config *Config) error {
	if len(config.FluentURL) > 0 {
		fluentHook, err := createFluentHook(config.FluentURL, config.Tag)
		if err != nil {
			return err
		}
		fluentHook.AddCustomizer(getFluentFormatCustomiser(config.TimestampFormat))
		fluentHook.SetLevels(getFluentLevels())
		logger.AddHook(fluentHook)
	} else {
		return errors.New(":FluentURL property of provided config does not contain a URL")
	}
	return nil
}

func New(config *Config) Logger {
	// set a default level
	if Levels[config.Level] == 0 {
		config.Level = DefaultLevel
	}

	// add the base tags
	if config.Fields["tag"] == nil {
		config.Fields["tag"] = config.Tag
	}

	// create the logger instance
	logger := &logrus.Logger{
		Formatter: &logrus.TextFormatter{
			CallerPrettyfier: prettifyCaller,
			DisableSorting:   true,
			FieldMap: logrus.FieldMap{
				logrus.FieldKeyFile:  config.FieldFile,
				logrus.FieldKeyFunc:  config.FieldFunc,
				logrus.FieldKeyLevel: config.FieldLevel,
				logrus.FieldKeyMsg:   config.FieldMessage,
				logrus.FieldKeyTime:  config.FieldTimestamp,
			},
			FullTimestamp:    true,
			QuoteEmptyFields: true,
			TimestampFormat:  config.TimestampFormat,
		},
		Level:        logrus.TraceLevel,
		Out:          os.Stdout,
		ReportCaller: true,
	}
	logger.Hooks = make(logrus.LevelHooks)

	return logger
}
